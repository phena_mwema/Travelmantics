# Travelmantics

An android mobile application that allows users to browse through a variety of posted holiday package deals at displayed resorts.

<img src="https://github.com/PhenaMwema/Travelmantics/blob/master/screenshots/Screenshot_20190804-221619.jpg" width="300">
<img src="https://github.com/PhenaMwema/Travelmantics/blob/master/screenshots/Screenshot_20190804-234233.jpg" width="300">
<img src="https://github.com/PhenaMwema/Travelmantics/blob/master/screenshots/Screenshot_20190804-234242.jpg" width="300">
<img src="https://github.com/PhenaMwema/Travelmantics/blob/master/screenshots/Screenshot_20190804-234251.jpg" width="300">
<img src="https://github.com/PhenaMwema/Travelmantics/blob/master/screenshots/Screenshot_20190804-234330.jpg" width="300">
